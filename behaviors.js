(function(){
  /**
   * Opens and closes the menu in smaller viewports by toggling the <<hidden >> class. The behavior of said class is
   * controlled purely by css.
   *
   * @param mainElem the menu item thet gets open or closed
   * @param openElem the button that triggers opening or closing
   */
  var mainElem = document.getElementById('menuC');
  var openElem = document.getElementById('menuB');
  function openClose(){
    mainElem.classList.toggle("hidden");
  }
  openElem.addEventListener('click', openClose);
  /**
   * Makes it possible to slide the quotes on the homepage by applying a negative margin to the first child element
   * of the quote container and displays a different number of them according to the currently applied media query.
   *
   * @param currentX holds the (negative) distance in pixels from the left border of the page to the first element
   * @param curElem holds a reference to the current leftmost element to position it @ 0 in case of a
   * viewport size change
   * @param goLeft holds a reference to the left arrow
   * @param goRight holds a reference to the right arrow
   */
  var currentX = 0;
  var curElem = 0;
  var goLeft = document.getElementById('L_arrow');
  var goRight = document.getElementById('R_arrow');
  function sideSlide(evt) {
    var slideCont = document.getElementById('homeQuotes');
    var slideFirst = slideCont.children[2];
    var slideElems = slideCont.children.length - 2;
    var backA = slideCont.children[0];
    var fwdA = slideCont.children[1];
    var getWidth = function() {
      return slideFirst.getBoundingClientRect().width;
    };
    var getMedia = function() {
      var mobW = window.matchMedia('(max-width:550px)');
      var tabW = window.matchMedia('(max-width:900px)');
      var bigW = window.matchMedia('(min-width:1400px)');
      if (mobW.matches) {
        return slideElems - 1;
      }
      else if (tabW.matches) {
        return slideElems - 2;
      }
      else if (bigW.matches) {
        return slideElems - 4;
      }
      else {
        return slideElems - 3;
      }
    };
    function applyNegM() {
      if (curElem === 0) {
        backA.style.display = 'none';
      }
      else {
        backA.style.display = 'block';
      }
      if (curElem === getMedia()) {
        fwdA.style.display = 'none';
      }
      else {
        fwdA.style.display = 'block';
      }
      slideFirst.style.marginLeft = '-' + currentX + 'px';
    }
    if (evt.target.id === 'L_arrow'&& curElem >0) {
      currentX -= getWidth();
      curElem --;
      applyNegM();
    }
    if (evt.target.id === 'R_arrow' && curElem < getMedia()) {
      currentX += getWidth();
      curElem ++;
      applyNegM();
    }
    if (evt.target === window) {
      if(curElem > getMedia()){
        curElem = getMedia();
      }
      currentX =getWidth()*curElem;
      applyNegM();
    }
  }
  if(goLeft) {
    goLeft.addEventListener("click", sideSlide);
  }
  if(goRight) {
    goRight.addEventListener("click", sideSlide);
  }
  window.addEventListener("resize", sideSlide);
  /**
   * Hides / shows answers in the FAQ section based on the fact that the clickable button that triggers the function,
   * with classname showArrow(when the answer is hidden) or hideArrow (when shown), always has the same relationship
   * with the answer container with class name shownAnswer / hidnAnswer depending on the current status
   *
   * @param evt a reference to the event object passed when the function is triggered
   *
   */
  function showHide(evt){
    var clickedElem = evt.target;
    if(evt.target.className === "showArrow"){
      clickedElem.parentNode.nextElementSibling.className = 'shownAnswer';
      clickedElem.className = 'hideArrow';
    }
    else if(evt.target.className === "hideArrow"){
      clickedElem.parentNode.nextElementSibling.className = 'hidnAnswer';
      clickedElem.className = 'showArrow';
    }
  }
  var shoFaq = document.getElementById('faq');
  if(shoFaq){
    shoFaq.addEventListener("click", showHide);
  }
  /**
   * Validates captcha on the client side, if the user hasn't solved the captcha triggers and error message that
   * lasts for 1.5 seconds and prevents the form from being submitted.
   *
   * @param submitMe a reference to the form element
   * @param errorMsg a reference to the hidden error message
   * @param captchaBox a reference to the captcha element
   *
   */
  var submitMe = document.getElementById('cform');
  var errorMsg = document.getElementById('captchaError');
  var captchaBox = document.querySelector('.g-recaptcha');
  /**
   * Validates Phone number on the client side, only checks for valid characters (digits, plus sign parenthesis, dot).
   *
   * @param input the user entered characters for the phone field
   *
   */
  var phoneNum = document.getElementById('phone');
  function validateTel() {
    if (phoneNum.value && phoneNum.value.replace(/\D/g, '').length >= 10){
      console.log('yep');
      phoneNum.setCustomValidity('');
    }
    else{
      console.log('nope');
      phoneNum.setCustomValidity('Please enter a valid US phone number');
    }
  }
  function validateMe(evt) {
    var captchaResp = grecaptcha.getResponse();
    if (captchaResp === ''){
      errorMsg.style.display = 'block';
      startFadeError();
      evt.preventDefault();
    }
  }
  function startFadeError() {
    var vanishMsg = window.setTimeout(function() {
      errorMsg.style.display = 'none';
    }, 1500);
  }
  phoneNum && phoneNum.addEventListener('blur',validateTel);
  submitMe.addEventListener('submit',validateMe);
})();

